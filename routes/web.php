<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//menambahkan database
Route::get('produk/add1', function(){
  $produk = new App\produk;
  $produk->nama_barang = "Pasta Gigi";
  $produk->harga_jual = 12000;
  $produk->diskon = 1500;
  $produk->save();
});
Route::get('produk/update1', function(){
  $produk = App\Produk::find(1);
  $produk->harga_jual = 200000;
  $produk->diskon = 2000;
  $produk->update();
});
Route::get('produk/delete1', function(){
  $produk = App\produk::find(2);
  $produk->delete();
});
Route::get('/kategori', function(){
  $kategori = App\Kategori::where('id_kategori','=', 3) -> first();
  echo "Produk untuk Kategori " . $kategori -> nama_kategori. ":";
  foreach ($kategori->produk as $data ) {
    echo "<li>" . $data->nama_barang."</li>";
  }
});
Route::get('selamat', function(){
  $produk =  App\Produk::where('id_produk', '>' , 1) -> get();
  return view ('selamat', ['nama' => 'Ichbal Yuda Backtiar', 'produk'=>$produk]);
});
Route::get('halaman1', function(){
  $title = 'Ini halaman pertama';
  $content = 'Anda sedang berada pada halaman pertama website kami';
  return view('konten.halaman1', compact('title', 'content'));
});
Route::resource('produk', 'ProdukController');

Route::get('pdfproduk', 'ProdukController@makePDF');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
