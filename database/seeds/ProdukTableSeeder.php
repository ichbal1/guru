<?php

use Illuminate\Database\Seeder;

class ProdukTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('produk')->insert(array(['nama_barang'=>'Bon Cabe', 'harga_jual'=>'10000', 'diskon'=>'0']));
    }
}
