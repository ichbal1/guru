<h2>Daftar Produk</h2>
<table border="1" cellspacing="0" >
    <tr>
      <th width="25%" > ID </th>
      <th width="50%" > Nama Produk </th>
      <th width="75%" > Harga </th>
    </tr>
    @foreach($produk as $data)
    <tr>
      <td align="center"> {{ $data -> id_produk }} </td>
      <td> {{ $data -> nama_barang }} </td>
      <td> {{ $data -> harga_jual }} </td>
    </tr>
    @endforeach
</table>
