<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    //
    //$protected $table = 'produk';
    protected $table = 'produk';
    protected $primaryKey = 'id_produk';
    protected $fillable = ['nama_barang', 'harga_jual', 'diskon'];

    public function kategori(){
      return $this->belongsTo('App\Kategori');
    }
}
