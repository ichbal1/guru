<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use Redirect;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     //deklarasi properti
     protected $pesan = array('nama.required '=> 'Isi nama produk' , 'kategori.required' => 'Pilih kategori',
        'harga.required' => 'Isi harga jual', );
     protected $aturan = array('nama' => 'required',
        'kategori' => 'required',
         'harga' => 'required', );
    public function index()
    {
        //yang akan dipanggil ketika action tidak disebutkan
        //$produk = Produk::all();
        //return view('produk', ['produk' => $produk]);

        $batas =5;
        $produk = Produk::join('kategori', 'kategori.id_kategori', '=', 'produk.id_kategori')
        ->orderBy('produk.id_produk', 'desc')
        ->paginate($batas);

        $kategori = Kategori::all();

        $no = $batas * ($produk->currentPage() - 1);
        return view('produk.index', compact('produk', 'no', 'kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //untuk menambah data pada produk
        $kategori = Kategori::all();
        return view('produk.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //menyimpan data
        $this->validate($request, $this->aturan, $this->pesan);

        $produk = new Produk;
        $produk->nama_barang = $request['nama'];
        $produk->id_kategori = $request['kategori'];
        $produk->harga_jual = $request['harga'];
        $produk->diskon = $request['diskon'];
        $produk->save();

        return Redirect::route('produk.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //untuk edit
        $kategori = Kategori::all();
        $produk = Produk::find($id);
        return view('produk.edit', compact('kategori', 'produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //menyimpan hasil update
        $this->validate($request, $this->aturan, $this->pesan);

        $produk = Produk::find($id);
        $produk->nama_barang = $request['nama'];
        $produk->id_kategori = $request['kategori'];
        $produk->harga_jual = $request['harga'];
        $produk->diskon = $request['diskon'];
        $produk->update();

        return Redirect::route('produk.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //untuk hapus data
        $produk = Produk::find($id);
        $produk->delete();
        return Redirect::route('produk.index');

    }

    public function makePDF(){
      $produk = Produk::join('kategori', 'kategori.id_kategori', '=', 'produk.id_kategori')
      -> orderBy('produk.id_produk', 'desc')->get();

      $no = 0;
      $pdf = 'PDF'::loadView('produk.pdf', compact('produk', 'no'));

      return $pdf->download();
    }
}
